#include "csapp.h"

void echo(int connfd);
void sighandler(int sig);
void trim(char *str);
int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];
	Signal(SIGCHLD,sighandler);
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		
		
		while(1){
			connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

			/* Determine the domain name and IP address of the client */
			hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
						sizeof(clientaddr.sin_addr.s_addr), AF_INET);
			haddrp = inet_ntoa(clientaddr.sin_addr);
			printf("server connected to %s (%s)\n", hp->h_name, haddrp);
			if (Fork()==0)
			{
				Close(listenfd);
				echo(connfd);
				Close(connfd);
				exit(0);
			}		
		Close(connfd);
		}
	}
}

void sighandler(int sig){
	while(waitpid(-1,0,WNOHANG)>0);
	return;
}

void echo(int connfd)
{
	int status;
	size_t n;
	char buf[MAXLINE];
	char comando[MAXLINE];
	rio_t rio;
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		strcpy(comando,buf);
		trim(comando);
		if (Fork()==0)
		{
			char *afirmativo = "OK\n";
			char *negativo = "ERROR\n";
			printf("%s\n", comando);
			if((status=system(comando))==0){
				Rio_writen(connfd, afirmativo, strlen(afirmativo));
			}else{
				Rio_writen(connfd, negativo, strlen(negativo));	
			}
			printf("status: %d\n", status);
			exit(0);
			
		}
	}

			


	
}


void trim(char *str)
{
    char *start, *end;

    /* Find first non-whitespace */
    for (start = str; *start; start++)
    {
        if (!isspace((unsigned char)start[0]))
            break;
    }

    /* Find start of last all-whitespace */
    for (end = start + strlen(start); end > start + 1; end--)
    {
        if (!isspace((unsigned char)end[-1]))
            break;
    }

    *end = 0; /* Truncate last whitespace */

    /* Shift from "start" to the beginning of the string */
    if (start > str)
        memmove(str, start, (end - start) + 1);
}
